import React from "react";
import styles from "./PeopleRow.module.css";
import Button from "../button";
import Avatar from "./components/avatar";
import { Data } from "../../data/mocks";
import MutualConnections from "./components/mutual-connections";

type PeopleRowProps = {
  data: Data;
};

const PeopleRow: React.FunctionComponent<PeopleRowProps> = ({ data }) => {
  return (
    <div className={styles["people-row"]}>
      <div className={styles.general}>
        <button className={styles.close} />
        <div className={styles["general-inner"]}>
          <Avatar
            url={data.avatarUrl}
            progress={data.matchPercentage}
            size="big"
          />
          <h3 className={styles.name}>{data.name}</h3>
          <h4 className={styles.location}>{data.location}</h4>
          <p className={styles.about}>{data.about}</p>
        </div>
        <footer className={styles.footer}>
          <Button type="pass" />
          <Button type="shortlist" />
        </footer>
      </div>
      <div className={styles.details}>
        <div className={styles.experience}>
          {Array(data.jobs.length * 3)
            .fill(data.jobs[0])
            .map((item, idx) => (
              <div key={idx} className={styles["experience-item"]}>
                <h3 className={styles.title}>
                  {item.title}
                  <span className={styles.production}>
                    {" "}
                    - {item.production}
                  </span>
                </h3>
                <span className={styles.year}>{item.year}</span>
                <span className={styles.genre}>{item.genre}</span>
                <span className={styles.genre}>{item.channel}</span>
              </div>
            ))}
        </div>
        <footer>
          <div className={styles["footer-item"]}>
            <span>mutual connections</span>
            <MutualConnections
              data={data}
              items={Array(8).fill(data.mutualAvatarUrl)}
            />
          </div>

          <div className={styles["footer-item"]}>
            <span>relevant credits</span>
            <span className={styles.counter}>{data.relevantCreditsCount}</span>
          </div>
        </footer>
      </div>
    </div>
  );
};

export default PeopleRow;
