import React from "react";
import styles from "./MutualConnections.module.css";
import Avatar from "../avatar";
import { Data } from "../../../../data/mocks";

type MutualConnectionsProps = {
  items: string[];
  data: Data;
};

const SHOW_MAX = 5;

const MutualConnections: React.FunctionComponent<MutualConnectionsProps> = ({
  items,
  data
}) => {
  return (
    <div className={styles["mutual-conntections"]}>
      {items.slice(0, SHOW_MAX).map((_item, idx) => (
        <div className={styles.item} key={idx}>
          <Avatar url={data.mutualAvatarUrl} size="small" />
        </div>
      ))}
      {items.length > SHOW_MAX && (
        <div className={styles["item-last"]}>+{items.length - SHOW_MAX}</div>
      )}
    </div>
  );
};

export default MutualConnections;
