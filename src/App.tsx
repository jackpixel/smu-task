import React from 'react';
import PeopleRow from './components/people-row';
import { data } from './data/mocks';

function App() {
  return (
    <div className="App">
      <PeopleRow data={data} />
    </div>
  );
}

export default App;
