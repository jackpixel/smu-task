type Job = {
  title: string;
  production: string;
  year: number;
  genre: string;
  channel: string;
};

export type Data = {
  avatarUrl: string;
  matchPercentage: number;
  name: string;
  location: string;
  about: string;
  jobs: Job[];
  mutualAvatarUrl: string;
  relevantCreditsCount: number;
};

export const data: Data = {
  avatarUrl: "https://via.placeholder.com/74",
  matchPercentage: 70,
  name: "Richard Smith",
  location: "Los Angeles, CA",
  about: `I’m an experienced editor and I bring talent and a good attitude to the edit.`,
  jobs: [
    {
      title: "Line Producer",
      production: "Big Brother (Season 8)",
      year: 2019,
      genre: "TV Reality/Doc",
      channel: "CBS - Our House Productions"
    }
  ],
  mutualAvatarUrl: "https://via.placeholder.com/24",
  relevantCreditsCount: 12
};
